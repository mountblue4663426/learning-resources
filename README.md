# Learning Resources

## Git
* [ProGit](https://git-scm.com/book/en/v2) _-Scott Chacon and Ben Straub_
* [Branching](https://learngitbranching.js.org/) : interactive web game.

## Javascript
* [Javascript.info](https://javascript.info)
* [You Dont Know Javascript](https://github.com/getify/You-Dont-Know-JS) -_Kyle Simpson_

## HTML-CSS
* [learn.shayhowe.com](https://learn.shayhowe.com)
* [Responsive Web Design](https://archive.org/details/responsivewebdesign) -_Ethan Marcotte_

